require 'rspec'
require 'w1d1_array'


describe Array do
  describe '#my_uniq' do
    original = [1,2,1,3,3]
    it 'returns a new array' do
      expect(original.my_uniq).not_to be original
    end

    it 'contains unique elements in the order they appeared' do
      expect(original.my_uniq).to eq([1,2,3])
    end
  end


  describe '#two_sum' do
    array = [-1, 0, 2, -2, 1]
    it 'returns an array' do
      expect(array.two_sum).to be_a(Array)
    end

    it 'returns an array where each element is an array of positions' do
      expect(array.two_sum[0]).to be_a(Array)
    end

    it 'each pair of posistions returned adds to zero' do
      expect(array.two_sum).to eq([[0,4],[2,3]])
    end

    it 'each pair contains uniq positions' do
      expect(array.two_sum.include?([1,1])).to be false
    end

    it 'pairs are sorted "dictionary-wise"' do
      array2 = [-1,1,1]
      expect(array2.two_sum).to eq([[0,1],[0,2]])
    end
  end

  describe '#my_transpose' do

    rows = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8]
      ]
    cols = [
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8]
      ]
    it 'returns a new object' do
      expect(rows.my_transpose).not_to be rows
    end

    it 'returns an array' do
      expect(rows.my_transpose).to be_a(Array)
    end

    it 'transposes original array' do
      expect(rows.my_transpose).to eq(cols)
    end

  end

end


describe "stock_picker()" do
  stock_prices = [4,7,9,5,13,4,11,1]
  it "returns an array of days" do
    expect(stock_picker(stock_prices)).to be_a(Array)
  end

  it "returns an array of 2 elements" do
    expect(stock_picker(stock_prices).count).to eq(2)
  end

  it "returns most profitable pair" do
    expect(stock_picker(stock_prices)).to eq([0,4])
  end
end

describe "TowersOfHanoi" do
  subject(:game) {TowersOfHanoi.new(3)}

  describe "initialize" do
    it "should initialize won to false" do
      expect(game.won).to be false
    end

    it "should initialize stack1" do
      expect(game.stack1).to eq([3, 2, 1])
    end

    it "should initialize winning_pos" do
      expect(game.winning_pos).to eq([3, 2, 1])
    end
  end

  describe "render" do
    it "shows all 3 stacks" do
      expect(game.render).to eq("[3, 2, 1], [], []")
    end
  end

  describe "move(start, end)" do
    before do
      game.move(1,2)
    end

    it "removes the top disk from start stack" do
      expect(game.stack1).to eq([3, 2])
    end

    it "pushes the removed disk to end stack"  do
      expect(game.stack2).to eq([1])
    end

    it "raises an exception if user tries to place larger disk on smaller" do
      expect do game.move(1,2) end.to raise_error(RuntimeError, "Cannot move disk there")
    end

  end

  describe "won?" do

    it "returns true" do
      game.stack1 = []
      game.stack3 = [3, 2, 1]

      expect(game.won?).to eq true
    end
  end


end