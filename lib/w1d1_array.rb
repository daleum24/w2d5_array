class Array
  def my_uniq
    new_array = []
    self.each do |el|
      new_array << el unless new_array.include?(el)
    end
    new_array
  end

  def two_sum
    pairs = []
    i = 0
    while i < self.length
      j = i + 1
      while j < self.length
        pairs << [i,j] if self[i] + self[j] == 0
        j+=1
      end
      i+=1
    end
    pairs
  end

  def my_transpose
    transposed_arr = Array.new(3) { Array.new(3,nil) }
    self.each_with_index do |row, row_i|
      row.each_with_index do |col, col_i|
        transposed_arr[row_i][col_i] = self[col_i][row_i]
      end
    end
    transposed_arr

  end

end

def stock_picker(stock_prices)
  best_pair = []
  profit = 0
  i = 0
  while i < stock_prices.length
    j = i + 1
    while j < stock_prices.length
      if stock_prices[j] - stock_prices[i] > profit
        profit = stock_prices[j] - stock_prices[i]
        best_pair = [i,j]
      end
      j+=1
    end
    i+=1
  end
  best_pair

end

class TowersOfHanoi
  attr_accessor :stack1, :stack2, :stack3, :won
  attr_reader :winning_pos

  def initialize(num)
    @stack1 = (1..num).to_a.reverse!
    @stack2 = []
    @stack3 = []
    @winning_pos = (1..num).to_a.reverse!
    @won = false
  end

  def move(start, end_pos)
    key = {1 => self.stack1, 2 => self.stack2, 3 => self.stack3}
    current_disk = key[start].pop

    if key[end_pos].empty?
      key[end_pos] << current_disk
    elsif key[end_pos][-1] > current_disk
      key[end_pos] << current_disk
    else
      key[start] << current_disk
      raise "Cannot move disk there"
    end
  end

  def render
    "#{self.stack1}, #{self.stack2}, #{self.stack3}"
  end

  def won?
    self.stack3 == self.winning_pos
  end

end

